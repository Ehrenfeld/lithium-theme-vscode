# Change Log
All notable changes to the "Lithium" extension will be documented in this file.

<!--Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.-->

## [Unreleased]
- Color in menu
- Color menu bar
- Notification color

## [6.12.0] - 2018-12-20
### Changed
- Update and improvement of general color
- Color of "editor.background"
- Color of "sideBarTitle.foreground"

## [6.12.2] - 2018-12-22
### Fixed
- Total debug

### Added
- README.md (with screenshot)

## [6.12.3] - 2018-12-29
### Added
- LICENSE.txt (GNU GLP v.3)

## [6.12.4] - 2018-12-30
### Added
- CHANGELOG.md

## Update for new year ! [6.13.2]
### Added
- All background color
- Update screenshot in README
- *and Happy New Year*

---------------------------
*Lithium - Theme* &copy; **GNU GLP v3 - 2007**