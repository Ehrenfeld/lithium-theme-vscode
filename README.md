# Lithium - **Relaxing** Theme for **Relaxed** people

Lithium **Relax** Theme for relaxed people. *Sweet* for the eyes.
Code and **Relax**.
For: All language (JS, PHP, HTML, CSS, SQL, JSON, JQUERY, SASS, ... )

## __Example__

### __MarkDown__

![markdown](https://zupimages.net/up/19/01/v2t3.png)

### __PHP__

![php](https://zupimages.net/up/19/01/uzbm.png)


### __Python__

![python](https://zupimages.net/up/19/01/zdwn.png)

### __SQL__

![sql](https://zupimages.net/up/19/01/eef4.png)

### For more information
* [GitLab here](https://gitlab.com/Ehrenfeld/lithium-theme-vscode)
* [MarketPlace VSCode](https://marketplace.visualstudio.com/items?itemName=Ehrenfeld.lithium)
* **If you can help me on my project, you can on my BTC Wallet:** *14gZxZPeypaELkGdTexRRQs4msaMx4bBsc*


**Tank you and Enjoy !**

* *Lithium - Theme* &copy; **GNU GLP v3 - 2007**
